$(document).ready(function(){
    $('.seat').on('click', function(){
        if(!$(this).hasClass('reserved')){
            $(this).toggleClass('selected');

            var clickedSeat = $(this).attr('seat_number');
            var currentSelectedSeatsJSON = $('input[name*=seat_id_seq]').val();
            var currentSelectedSeats = (currentSelectedSeatsJSON != undefined && currentSelectedSeatsJSON != "") ? JSON.parse(currentSelectedSeatsJSON) : [];

            var currentSeatIndex = $.inArray(clickedSeat, currentSelectedSeats);
            
            if(currentSeatIndex >= 0){
                currentSelectedSeats.splice(currentSeatIndex,1);
            }else{
                currentSelectedSeats.push(clickedSeat);
            }

            $('input[name*=seat_id_seq]').val(JSON.stringify(currentSelectedSeats));
            $('input[name*=price]').val($('input[name*=price]').attr('cost_per_ticket') * currentSelectedSeats.length);
        }
    })

    if($('input[name*=seat_id_seq]').length > 0){
        var currentSelectedSeatsJSON = $('input[name*=seat_id_seq]').val();
        var currentSelectedSeats = (currentSelectedSeatsJSON != undefined && currentSelectedSeatsJSON != "") ? JSON.parse(currentSelectedSeatsJSON) : [];
        
        if(currentSelectedSeats.length > 0){
            $.each(currentSelectedSeats,function(key, value){
                $('.seat[seat_number='+value+']').addClass('selected');
            })

            $('input[name*=price]').val($('input[name*=price]').attr('cost_per_ticket') * currentSelectedSeats.length);
        }
    }
})