class EventsController < ApplicationController
  include EventsHelper

  before_action :set_event, only: [:show, :edit, :update]
  # before_action :check_logged_in, :only => [:new, :create]
  before_action :check_if_admin?, :only => [:edit, :update]
  before_action :authenticate_user!, :except => [:show, :index]
  before_action :checkEighteenYearsOld, :only => [:show]

  def index
    @events = Event.all
  end

  def show
    @reservedPlaces = Ticket.where(event_id: params[:id]).where(status: [1,2]).pluck(:seat_id_seq, :id)
  end

  def edit
  end

  def new
    @event = Event.new
  end

  def update
    respond_to do |format|
      if @event.update(events_params)
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  def newTicket
    @ticket = Ticket.new(:event_id => params[:id])
  end

  def createTicket
    @postValues = ticket_params
    @event = Event.find(@postValues["event_id"])
    @pricePerTicket = calculateEventPrice(@event)
    if (@postValues["seat_id_seq"] != "")
      @seatsArray  = JSON.parse @postValues["seat_id_seq"]
    else
      @seatsArray = []
    end
    
    @currentUserTickets = Ticket.where(user_id: current_user.id).where(event_id: @event.id).where(status: [1,2]).count

    # Ticket for validation
    @ticket = Ticket.new(ticket_params)
    @ticket.user_id = current_user.id

    respond_to do |format|
      if @ticket.validate && current_user.wallet != nil && current_user.wallet >= @seatsArray.count * @pricePerTicket && (@currentUserTickets + @seatsArray.count) <= 5 && @seatsArray.count > 0
          @seatsArray.each do |seat_id|
            @ticket = Ticket.new(ticket_params)
            @ticket.user_id = current_user.id

            @ticket.price = @pricePerTicket
            @ticket.seat_id_seq = seat_id
            @ticket.status = 1
            @ticket.save
          end

          current_user.wallet -= (@seatsArray.count * @pricePerTicket)
          current_user.save
          
        format.html { redirect_to Event.find(params[:id]), notice: 'Tickets bought successfully.' }
        format.json { render :show, status: :created, location: @ticket }
      else
        if current_user.wallet == nil || current_user.wallet < @seatsArray.count * @pricePerTicket
          @ticket.errors.add(:price, "is higher than your wallet")
        end

        if (@currentUserTickets + @seatsArray.count) > 5
          @ticket.errors.add(:seat_id_seq, "max allowed tickets per user are 5")
        end

        if @seatsArray.count == 0
          @ticket.errors.add(:seat_id_seq, "you have to choose the seat")
        end

        @ticket.seat_id_seq = @postValues["seat_id_seq"]
        format.html { render :newTicket }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  def showTicket
  end

  def create
    @event = Event.new(events_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def check_logged_in
      authenticate_or_request_with_http_basic("Ads") do |username, password|
        username == "admin" && password == "admin"
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end
    
      # Never trust parameters from the scary internet, only allow the white list through.
      def events_params
        params.require(:event).permit(:artist, :description, :price_low, :price_high, :event_date, :eighteen_years_old, :image, :number_of_tickets, :start_selling_date)
      end
      
      # Never trust parameters from the scary internet, only allow the white list through.
      def ticket_params
        params.require(:ticket).permit(:name, :seat_id_seq, :address, :price, :email_address, :phone, :event_id)
      end

      def check_if_admin?
        redirect_to root_path unless current_user.admin? 
      end

      # Check if user got access to see the event
      def checkEighteenYearsOld
        @event = Event.find(params[:id])
        redirect_to root_path unless (!@event.eighteen_years_old || (user_signed_in? && current_user.eighteen_years_old))
      end
end
