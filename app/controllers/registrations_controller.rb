class RegistrationsController < Devise::RegistrationsController
    private
    def sign_up_params
        params.require(:user).permit(:email, :password, :password_confirmation, :eighteen_years_old)
    end
    
    def account_update_params
        params.require(:user).permit(:email, :password, :password_confirmation, :current_password, :eighteen_years_old)
    end
end