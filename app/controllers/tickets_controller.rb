class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]
  before_action :check_if_admin?, :only => [:showAllTickets, :returnTicket]

  include EventsHelper
  # GET /tickets
  # GET /tickets.json
  def index
    @tickets = Ticket.all
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new
  end

  # GET /tickets/1/edit
  def edit
  end

  def showAllTickets
    @tickets = Ticket.all.order(created_at: :desc)
  end

  def returnTicket
    @ticket = Ticket.find(params[:id])
    @event = Event.find(@ticket.event_id)
    @user = User.find(@ticket.user_id)

    @userFine = calculateTheFine(@event, @ticket.updated_at.to_date)

    @user.wallet = (@user.wallet || 0) + (@ticket.price * (100 - @userFine))/100
    @ticket.status = 3

    respond_to do |format|
      if calculateDaysToEvent(@event, @ticket.updated_at.to_date) > 0 && @user.save && @ticket.save
        format.html { redirect_to admin_tickets_path, notice: "Ticket was returned successfully."}
        format.json { head :no_content }
      else
        format.html { redirect_to admin_tickets_path, alert: "You cannot return this ticket"}
        format.json { head :no_content }
      end
    end
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(ticket_params)

    if @ticket.price <= current_user.wallet
      @ticket.user_id = current_user.id
      @ticket.status = 1

      respond_to do |format|
        if @ticket.save
          format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
          format.json { render :show, status: :created, location: @ticket }
        else
          format.html { render :new }
          format.json { render json: @ticket.errors, status: :unprocessable_entity }
        end
      end
    else
      format.html { render :new }
      format.json { render json: @ticket.errors, status: :unprocessable_entity }
    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    respond_to do |format|
      if @ticket.update(ticket_params)
        format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.status = 2
    @ticket.save
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: 'Ticket return request has been sent successfully.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:name, :seat_id_seq, :address, :price, :email_address, :phone, :event_id)
    end
    
    def check_if_admin?
      redirect_to root_path unless current_user.admin? 
    end
end
