class UsersController < ApplicationController
    before_action :set_ticket, only: [:show, :edit, :update, :destroy]

  def wallet
  end

  def topUpWallet
    @user = current_user
    @user.wallet = (@user.wallet || 0) + (params[:amount]["amount"]).to_f
    @user.save

    respond_to do |format|
        format.html { redirect_to wallet_path, notice: "Wallet was successfully updated."}
        format.json { render :wallet, status: :ok, location: wallet_path }
    end
  end

  def showUserTickets
    @tickets = Ticket.where(user_id: params[:id])
  end
end