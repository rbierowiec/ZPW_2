module ApplicationHelper
    def currentPath(path)
        "active" if current_page?(path)
    end
end
