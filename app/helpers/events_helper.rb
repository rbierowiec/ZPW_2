module EventsHelper
    def calculateTheFine(event, fromDate)
        @daysToEvent = calculateDaysToEvent(event,fromDate)
        
        (@daysToEvent > 31) ? 10 : (@daysToEvent > 21) ? 20 : (@daysToEvent > 11) ? 30 : (@daysToEvent > 5) ? 40 : (@daysToEvent > 2) ? 50 : 60
    end

    def calculateEventPrice(event)
        @daysToEvent = calculateDaysToEvent(event, Date.today)
        
        (@daysToEvent > 31) ? event.price_low : (@daysToEvent > 21) ? event.price_low + (event.price_high - event.price_low)/8 : (@daysToEvent > 11) ? event.price_low + (event.price_high - event.price_low)/4 : (@daysToEvent > 5) ? event.price_low + (event.price_high - event.price_low)/2 : (@daysToEvent > 2) ? event.price_high : event.price_high * 1.20
    end

    def calculateDaysToEvent(event, fromDate)
        ((event.event_date || fromDate)  - fromDate).to_i
    end

    def ticketsLeft(event)
        (event.number_of_tickets || 0) - Ticket.where(event_id: event.id, status: [1,2]).count(:id)
    end
end
