class Event < ApplicationRecord
    has_many :tickets

    validates                           :image, attachment_presence: true
    has_attached_file                   :image, default_url: "/images/:style/missing.png"
    validates_attachment_content_type   :image, content_type: /\Aimage\/.*\z/
    validates_attachment_file_name      :image, matches: [/png\z/, /jpe?g\z/]

    validates   :artist, :presence => true
    validates   :description, :presence => true
    validate    :event_date_greater_than_today
    validates   :price_low, presence: true, format: { with: /\A\d+(?:\.\d{0,2})?\z/ }, numericality: { greater_than: 0, less_than: 1000000 }
    validates   :price_high, presence: true, format: { with: /\A\d+(?:\.\d{0,2})?\z/ }, numericality: { greater_than: 0, less_than: 1000000 }
    validate    :price_high_lower_than_price_low
    validates   :number_of_tickets, :presence => true, numericality: { greater_than: 0}
    validate    :start_selling_date_greater_than_today

    def price_high_lower_than_price_low
        if price_low.present? && price_high.present? && price_low > price_high
            errors.add(:price_high, "can't be lower than price low")
        end
    end

    def start_selling_date_greater_than_today
        if !start_selling_date.present?
            errors.add(:start_selling_date, "can't be empty")
        end
        if start_selling_date.present? && start_selling_date < Date.today
            errors.add(:start_selling_date, "can't be in the past")
        end
        if start_selling_date.present? && event_date.present? && start_selling_date > event_date
            errors.add(:start_selling_date, "can't be after the concert date")
        end
    end    

    def event_date_greater_than_today
        if !event_date.present?
            errors.add(:event_date, "can't be empty")
        end
        if event_date.present? && event_date < Date.today
            errors.add(:event_date, "can't be in the past")
        end
    end    
end
