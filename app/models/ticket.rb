class Ticket < ApplicationRecord
    belongs_to :event
    belongs_to :user

    validates :phone, :presence => true, format: {with: /\A[+-]?\d+\z/}
    validates_length_of :phone, :minimum => 7, :maximum => 13
    validates :email_address, :presence => true
    validates :price, presence: true, format: { with: /\A\d+(?:\.\d{0,2})?\z/ }, numericality: { greater_than: 0 }
    
end