Rails.application.routes.draw do
  
  # devise_for :users
  devise_for :users, :controllers => { registrations: 'registrations' }
  # Events routes
  get 'events/index'
  get 'events/new'
  get 'events/:id' => 'events#show', as: 'event_show'
  patch 'events/:id' => 'events#update', as: 'event_update'
  get 'events/:id/edit' => 'events#edit', as: 'event_edit'
  get 'events/:id/newTicket', to: 'events#newTicket'
  post 'events/:id/newTicket', to: 'events#createTicket'

  get 'wallet', to: 'users#wallet'
  post 'wallet', to: 'users#topUpWallet'
  get 'user/:id/tickets', to: 'users#showUserTickets'
  
  get 'admin/tickets', to: 'tickets#showAllTickets'
  post 'admin/cancel/ticket/:id', to: 'tickets#returnTicket', as: 'admin_cancel_ticket'
  
  resources :events, :only => [:index, :new, :create, :show]
  resources :tickets
  
  # Set tickets as main page
  root :to => "events#index"
end
