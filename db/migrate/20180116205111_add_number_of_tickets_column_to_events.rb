class AddNumberOfTicketsColumnToEvents < ActiveRecord::Migration[5.1]
  def change
    change_table :events do |t|
      t.integer   :number_of_tickets
    end
  end
end
