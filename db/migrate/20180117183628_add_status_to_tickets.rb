class AddStatusToTickets < ActiveRecord::Migration[5.1]
  def change
    change_table :tickets do |t|
      t.integer   :status
    end
  end
end
