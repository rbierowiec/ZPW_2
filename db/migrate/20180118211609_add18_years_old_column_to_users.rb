class Add18YearsOldColumnToUsers < ActiveRecord::Migration[5.1]
  def change
    change_table :users do |t|
      t.boolean   :eighteen_years_old
    end
  end
end
