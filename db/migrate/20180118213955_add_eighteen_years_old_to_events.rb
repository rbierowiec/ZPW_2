class AddEighteenYearsOldToEvents < ActiveRecord::Migration[5.1]
  def change
    change_table :events do |t|
      t.boolean   :eighteen_years_old
    end
  end
end
