class AddSellingStartDateToEvents < ActiveRecord::Migration[5.1]
  def change
    change_table :events do |t|
      t.date   :start_selling_date
    end
  end
end
