$(document).ready(function(){
    $('.date_from, .date_to, .artist').on('change keyup', function(){
        var fromDate = $('.date_from').val();
        var toDate = $('.date_to').val();
        var artist = $('.artist').val().toLowerCase();

        $('.card').each(function(key, value){
            var eventDate = $(this).find('.eventDate').html();
            var eventArtist = $(this).find('.eventArtist').html().toLowerCase();

            if((fromDate == "" || fromDate <= eventDate) && (toDate == "" || toDate >= eventDate) && (artist == "" || eventArtist.indexOf(artist) >= 0)){
                $(this).show();
            }else{
                $(this).hide();
            }
        })
    });

    $('.showFilters').on('click', function(){
        if($(this).prop('checked')){
            $('.filtersContent').slideDown();
        }else{
            $('.filtersContent').slideUp();
        }
    });

    $('.seat').on('click', function(){
        if(!$(this).hasClass('reserved')){
            $(this).toggleClass('selected');

            var clickedSeat = $(this).attr('seat_number');
            var currentSelectedSeatsJSON = $('input[name*=seat_id_seq]').val();
            var currentSelectedSeats = (currentSelectedSeatsJSON != undefined && currentSelectedSeatsJSON != "") ? JSON.parse(currentSelectedSeatsJSON) : [];

            var currentSeatIndex = $.inArray(clickedSeat, currentSelectedSeats);
            
            if(currentSeatIndex >= 0){
                currentSelectedSeats.splice(currentSeatIndex,1);
            }else{
                currentSelectedSeats.push(clickedSeat);
            }

            $('input[name*=seat_id_seq]').val(JSON.stringify(currentSelectedSeats));
            $('input[name*=price]').val($('input[name*=price]').attr('cost_per_ticket') * currentSelectedSeats.length);
        }
    })

    if($('input[name*=seat_id_seq]').length > 0){
        var currentSelectedSeatsJSON = $('input[name*=seat_id_seq]').val();
        var currentSelectedSeats = (currentSelectedSeatsJSON != undefined && currentSelectedSeatsJSON != "") ? JSON.parse(currentSelectedSeatsJSON) : [];
        
        if(currentSelectedSeats.length > 0){
            $.each(currentSelectedSeats,function(key, value){
                $('.seat[seat_number='+value+']').addClass('selected');
            })

            $('input[name*=price]').val($('input[name*=price]').attr('cost_per_ticket') * currentSelectedSeats.length);
        }
    }
})